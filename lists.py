my_lists = ['Evander', 22, True]
print(my_lists)

days_of_the_week = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
]
print(days_of_the_week)
print("The first day of the week is", days_of_the_week[0])

days_of_the_week[3] = "Hump day!"
days_of_the_week[5] = "FriYAY!"
print(days_of_the_week)

favorite_foods = []
food = input("What is one of your favorite foods? ")
favorite_foods.append(food)

food = input("What is one of your favorite foods? ")
favorite_foods.append(food)

food = input("What is one of your favorite foods? ")
favorite_foods.append(food)

print("Your fav foods are", favorite_foods)

num_foods = len(favorite_foods)
print("That's", num_foods, "of your favorite foods.")
